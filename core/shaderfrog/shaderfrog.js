import parse from 'glsl-parser/direct';
import deparse from '@andrewray/glsl-deparser';
import { mergeAsts, emptyAst } from '../ast-utils/ast-utils';
import { findFillerNodes } from '../frog-graph-utils/frog-graph-utils';
import {
  generateFnParams,
  findAstNode,
  findAstNodes,
  renameToken,
  replaceNodeInAst
} from '../ast-utils/ast-utils';
import { ky, vl } from '../utils/utils';

export const fillHole = (hole, childNode, astToFill, fragmentAst) => {
  const {
    // What we fill our hole with, like main_2()
    fillerAst: childFillerAst,
    // What we concat with our shader
    fragmentAst: childFragmentAst
  } = fillHoles(childNode);

  const isReplaceArgs =
    hole.type === 'replace call' &&
    (hole.keepArgs.length || hole.removeArgs.length);

  // Walk the frog graph until we find a shader (it might be the first one)
  const childShaderNodes = findFillerNodes(
    childNode,
    ({ type }) => type === 'shader'
  );
  const childShaderNode = childShaderNodes[0];

  const { fillerInstruction, exportAst } = childShaderNode;

  if (isReplaceArgs && !childShaderNode) {
    console.warn(
      `No suitable filler shader was found to plug arguments in to!`
    );
  }
  if (isReplaceArgs && childShaderNode) {
    const { index: childIndex, exportAst: childExportAst } = childShaderNode;

    // Replace all variables in the child shader named the user defined
    // replacer. Keep track of the variable name so we can put it in the
    // function signature. Example of keepArgs:
    //  [{ 1: { backfill: { name: 'vUv', type: 'vec2' } }],
    const backFillArgs = hole.keepArgs
      .filter(arg => typeof arg === 'object')
      .map(obj => vl(obj).backfill);

    const { ast: childExportFilled, newArgs } = backFillArgs.reduce(
      ({ ast, newArgs }, { name, type }) => {
        let varName, original;
        const withBackfiled = findAstNodes(
          ast,
          node => node.token.descoped === name
        ).reduce(
          (ast, astNode) =>
            renameToken(
              ast,
              astNode,
              (varName = `${(original = astNode.data)}_backfilled`)
            ),
          ast
        );
        return {
          ast: withBackfiled,
          newArgs: [...newArgs, { keep: original, mangled: varName, type }]
        };
      },
      {
        ast: childExportAst,
        newArgs: []
      }
    );

    // Replace the arguments of the child function call with our own
    const childExportWithBackports = replaceNodeInAst(
      childExportFilled,
      findAstNode(childExportFilled, ({ type }) => type === 'functionargs')
        .children,
      generateFnParams(
        newArgs.map(({ type, mangled }) => `${type} ${mangled}`).join(', ')
      ).children
    );

    const mangledChildFragmentAst = replaceNodeInAst(
      childFragmentAst,
      childExportAst,
      childExportWithBackports
    );

    const parentNodeToReplace = findAstNode(
      childFillerAst,
      ({ type }) => type === 'call'
    );

    // Get all arguments to the function call hole we're replacing
    // "keep arguments" are either indexes like [1,3], or what to rename the
    // value in the filler node if any variables match it, like [{1: {...}}]
    const keepIndices = hole.keepArgs.map(arg =>
      typeof arg === 'number' ? arg : parseInt(ky(arg))
    );
    const args = hole.expression.children.filter((_, index) =>
      keepIndices.length
        ? keepIndices.includes(index - 1)
        : index !== 0 && !hole.removeArgs.includes(index - 1)
    );

    const whatIsThis = replaceNodeInAst(
      childFillerAst,
      parentNodeToReplace.children,
      [...parentNodeToReplace.children, ...args]
    );

    return {
      // Replace our own hole with what the child offered up to us
      astToFill: replaceNodeInAst(astToFill, hole.expression, whatIsThis),
      // Merge in the full program the child offered up to us
      fragmentAst: mergeAsts(fragmentAst, mangledChildFragmentAst)
    };
  } else {
    return {
      // Replace our own hole with what the child offered up to us
      astToFill: replaceNodeInAst(astToFill, hole.expression, childFillerAst),
      // Merge in the full program the child offered up to us
      fragmentAst: mergeAsts(fragmentAst, childFragmentAst)
    };
  }
};

export const fillHoles = frogNode => {
  const { holes, program, fillers, ast, fillerInstruction } = frogNode;

  // Bail if there's no filler for this hole
  const filled = Object.keys(holes)
    .filter(holeName => holeName in fillers)
    .reduce(
      ({ astToFill, fragmentAst }, holeName) =>
        fillHole(holes[holeName], fillers[holeName], astToFill, fragmentAst),
      {
        fragmentAst: emptyAst(),
        astToFill: ast
      }
    );

  if (
    fillerInstruction.type === 'inline fillerInstruction defined expression'
  ) {
    // If this node has a specific instruction to offer up to the parent as
    // a filler, like a shader offering up _main_1(), make sure the evolving
    // fragment contains our filled ast
    return {
      fragmentAst: mergeAsts(filled.fragmentAst, filled.astToFill),
      fillerAst: fillerInstruction.expression
    };
  } else if (fillerInstruction.type === 'inline filled in ast') {
    // If the parent is supposed to inline our filled in ast, like a+b,
    // offer up our filled in ast as the filler, and pass up the fragment
    // we calculated
    return {
      fragmentAst: filled.fragmentAst,
      fillerAst: filled.astToFill
    };
  } else {
    // The output node won't have a filler to offer up to a parent, it can
    // be discarded
    return {
      fragmentAst: mergeAsts(filled.fragmentAst, filled.astToFill),
      fillerAst: emptyAst()
    };
  }
};
