export const replaceExpression = expression => ({
  type: 'replace expression',
  expression
});

export const replaceCall = (
  expression,
  keepArgs = [],
  removeArgs = [],
  builtin = false
) => ({
  type: 'replace call',
  builtin,
  keepArgs,
  removeArgs,
  expression
});

export const replaceBuiltinCall = (...args) => replaceCall(...args, true);
