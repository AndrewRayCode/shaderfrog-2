// I wish i knew how to programme
export const collectFillerNodesByPredicate = (node, match, collection, max) => {
  if (match(node)) {
    collection.push(node);
  }
  if (collection.length === max) {
    return;
  }
  if (node.fillers) {
    const fillers = Object.values(node.fillers);
    for (let i = 0; i < fillers.length; i++) {
      collectFillerNodesByPredicate(fillers[i], match, collection);
      if (collection.length === max) {
        return true;
      }
    }
  }
};

export const findFillerNodes = (node, match, max = Infinity) => {
  const collection = [];
  collectFillerNodesByPredicate(node, match, collection, max);
  return collection;
};

export const findFillerNode = (node, match) => {
  if (match(node)) {
    return node;
  }
  const fillers = Object.values(node.fillers);
  for (let i = 0; i < fillers.length; i++) {
    let filler = fillers[i];
    if (match(filler)) {
      return filler;
    }
  }
};

export const categorizeAst = ast => ({
  ast,
  ...groupBy(
    ast.children,
    {
      precisions: node =>
        node.token.data === 'precision' && node.token.type === 'keyword',
      preprocessors: node => node.type === 'preprocessor'
    },
    'restStatements'
  )
});

export const mergeCategorized = (left, right) => ({
  precisions: [...left.precisions, ...right.precisions],
  preprocessors: [...left.preprocessors, ...right.preprocessors],
  restStatements: [...left.restStatements, ...right.restStatements],
  scope: { ...left.scope, ...right.scope }
});

export const composeMyFrogAsts = (base, ...rest) =>
  rest.reduce(mergeCategorized, base);

export const emptyFrogShader = () => ({
  precisions: [],
  preprocessors: [],
  restStatements: [],
  scope: {}
});

export const frogAstToProgram = (...nodes) => {
  const {
    mains,
    precisions,
    preprocessors,
    restStatements,
    scope
  } = nodes.reduce(
    (memo, node) => ({
      precisions: [...memo.precisions, ...node.precisions],
      preprocessors: [...memo.preprocessors, ...node.preprocessors],
      restStatements: [...memo.restStatements, ...node.restStatements],
      scope: { ...memo.scope, ...node.scope }
    }),
    emptyFrogShader()
  );

  const precisionsRanked = precisions.reduce((memo, stmt) => {
    const precisionData = stmt.children[0].children;
    const precisionType = precisionData[1].token.data;
    const precisionValue = precisionData[0].token.data;
    const rank = ['lowp', 'mediump', 'highp'].indexOf(precisionValue);
    const existing = memo[precisionType];
    return {
      ...memo,
      [precisionType]:
        existing && existing.rank >= rank
          ? existing
          : {
              rank,
              stmt
            }
    };
  }, {});

  const precisionStmts = Object.values(precisionsRanked).map(
    ({ stmt }) => stmt
  );

  const merged = {
    type: 'stmtlist',
    token: {
      type: '(program)',
      data: '(program)'
    },
    children: [...preprocessors, ...precisionStmts, ...restStatements],
    scope
  };

  return merged;
};

export const groupBy = (array, matchers, restName = 'rest') => {
  const keys = Object.keys(matchers);
  const baseGroups = keys.reduce((memo, key) => ({ ...memo, [key]: [] }), {
    [restName]: []
  });
  return array.reduce((memo, element) => {
    const matched =
      keys.find(matchKey => matchers[matchKey](element)) || restName;
    return {
      ...memo,
      [matched]: [...memo[matched], element]
    };
  }, baseGroups);
};
