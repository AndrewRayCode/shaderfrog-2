# Things to do

✅ Fix gl_FragColor not being renamed

- Fix precisions not being merged
- Make hole finding generic based on engine(?)
- Fix duplicate declarations of uniforms not being removed

# Nodes and Graph

We build a graph like

```js
const graph = outputNode('gl_FragColor', {
  output: shaderNode(
    MeshPhongMaterialFragment,
    'main',
    'gl_FragColor',
    {
      texture2D_0: addNode({
        0: shaderNode(RaysFragment, 'main', 'gl_FragColor', {}, 2),
        1: shaderNode(NoiseFragment, 'main', 'gl_FragColor', {}, 3)
      })
    },
    1
  )
});
```

The signature for shaderNode is

```js
const shaderNode = (source, mainFnName, outVar, fillers, index) = {
```

Then we call `fillHoles` on the graphs

Creating shaderNode parses the shader into an AST finds the "holes", the hole
finding is hard coded into the node type. (I think this could be more generic,
a node could take in a hole finding algorithm instead of hard coding it).

The holes found are named by a key. For Shaders, right now, it's hard coded
to find texture2D calls. For math nodes, like "a + b", it finds all identifiers.

# Filling Holes

A node also defines a "fillerInstruction", which tells the parent how to fill
in their hole with this node.

A "hole" is defined as an object which takes in a real AST node from the graph,
and some metadata about the hole.

Holes are defined in `core/frog-graph-utils/holes.js`.

What's "filled in" to the parent's hole could be:

- The full child AST, like a+b
- A brand new thing, like \_main_2(), the "filler defined expression"
- A specific part of the child AST program (this doesn't exist yet, and I think
  could just be the same as above)

# Backfilling

"Backfilling" is the process of taking a source shader, like

```javascript
void main( void )
{
	vec2 pos = vUv;
```

And when building the final shader, replacing any variables named something
specific (like vUv) with a function argument:

```javascript
vec4 _2_main(vec2 _2_vUv_backfilled) {
  vec2 pos = _2_vUv_backfilled;
```

A node also defines an "exportAst," which I don't remember why I made it, but
in the end this is what gets "backfilled." Like exportAst of a shader is the
main() function. I "backfill" the contents of this function, renaming variables
in it to be passed in function arguments. Backfilling might be the wrong term.

Right now each node type defines arg backfills, which seems wrong. In
`core/nodes/nodes.js`, the shaderNode says "define texture2D calls as holes,"
and when filling in this hole, take the child node, and

# Thoughts

Could I combine all ASTs up front first, if they're full shader programs?
Does renaming / combining variables at the top level require specific steps
at each shader merge, or could, at the top level, I combine all uniform names
by the engine type (like cameraPosition), and define some entry point function
as main to begin hole filling?

Would either of these strategies work better for fragment vs vertex?

Are there any other creative places I can fill in holes besides texture2D()?

Should I do the parallax mapping exercise to get an idea of where a hole might
be filled in?

If you have shader

```javascript
uniform float image;

void main() {
  vec3 color = texture2D(image, vUv).rgb;
  gl_FragColor = vec4(color, 1.0);
}
```

and you fill it in with

```javascript
uniform float myImage;

vec2 add() {
  return vUv * 2.0;
}

void main() {
  vec3 color = texture2D(myImage, vUv + add());
  gl_FragColor = vec4(color, 1.0);
}
```

you get

```javascript
uniform float image;

void main_1() {
  vec3 color = main_2(vUv).rgb;
  gl_FragColor = vec4(color, 1.0);
}

uniform float myImage_2;

vec2 add_2() {
  return vUv * 2.0; /*
    This won't be backfilled.
    Should this just be a global var replace?
    But then it won't take into account transformations of the father.
  */
}

void main_2(vec2 _vUv) {
  vec3 color = texture2D(_vUv + add());
  gl_FragColor = vec4(color, 1.0);
}

void main() {
  gl_FragColor = main_1();
}
```

# Multiple texture2D calls

The ThreeJS shader defines this:

```javascript
		float Hll = bumpScale * texture2D( bumpMap, vUv ).x;
		float dBx = bumpScale * texture2D( bumpMap, vUv + dSTdx ).x - Hll;
		float dBy = bumpScale * texture2D( bumpMap, vUv + dSTdy ).x - Hll;
```

This would require multile calls down to the original shader, and also multiple
holes to find and fill (and group). This would get super expensive!

This also all requires a shader to be backfilled _after_ its compiled in the
context of a Threejs engine. Like this:

```javascript
#ifdef USE_AOMAP
	float ambientOcclusion = ( texture2D( aoMap, vUv2 ).r - 1.0 ) * aoMapIntensity + 1.0;
	reflectedLight.indirectDiffuse *= ambientOcclusion;
	#if defined( USE_ENVMAP ) && defined( PHYSICAL )
		float dotNV = saturate( dot( geometry.normal, geometry.viewDir ) );
		reflectedLight.indirectSpecular *= computeSpecularOcclusion( dotNV, ambientOcclusion, material.specularRoughness );
	#endif
#endif
```

I can't fill in this AST until threejs runs. So shaderfrog needs to run on top
of existing shader code after it's been added to the scene, and somehow get
THREE global variables like that.

# ThreeJS shader buliding path

The reason we start with MeshPhongMaterial is because it sets a few properties
like lights = true that we'd have to find out either way.

- Make rawShaderMaterial
- Call shaderfrog hack before scene to:
  a. .... idea was to set up reference from material to object so shaderfrog
  onbeforecompile could set object to meshphongmaterial, call compile(), get
  source code, set back, but now in hell of every shaderfrog shader calling
  compile before?

could we add onbeforelink to threejs? not likely, its just string building

## `initMaterial()`:

- If detected change in `programChange`
- Then builds "materialProperties.shader" = {
  name: material.type,
  uniforms: cloneUniforms( shader.uniforms ),
  vertexShader: shader.vertexShader,
  fragmentShader: shader.fragmentShader"
- Then calls onBeforeCompile()
- generates un-replaced source code for shader
- Calls acquireProgram(), which under hood
  - now builds a new `WebGLProgram`
  - This builds fragment, vertex, adds all defines like `define USE_MAP` - AFTER
    the onbeforecompile has happened
- Then materialProperties.program.getUniforms() is called
- This calls gl.getProgramParameter and gl.getActiveUniform, returning

  ```ruby
  ["map", "modelViewMatrix", "ambientLightColor", "toneMappingExposure", "projectionMatrix", "shininess", "diffuse", "pointLights[0].position", "pointLights[0].color", "pointLights[0].distance", "pointLights[0].decay", "pointLights[0].shadow", "pointLights[0].shadowBias", "pointLights[0].shadowRadius", "pointLights[0].shadowMapSize", "pointLights[0].shadowCameraNear", "pointLights[0].shadowCameraFar", "uvTransform", "emissive", "opacity", "normalMatrix", "specular"]
  ```

  - From the docs, https://developer.mozilla.org/en-US/docs/Web/API/WebGLRenderingContext/getActiveUniform
  - after linkProgram called, webgl creates list of active uniforms
  - default uniforms are defined in src/renderers/shaders/ShaderLib.js

- At render time, `materialProperties.shader.uniforms` is what's passed into
  refreshUniformsCommon, and then refreshUniformsPhong. where are these set? - these are set in ShaderLib...phong

- WebGLUniforms.upload is called for each render
- This loops over uniformsList, which is composed of Three's SingleUniform.
  Each SingleUniform has an address

- MeshPhongMaterial, at least, is coupled to the object, because it calls
  allocateBones( object ) to get the bones

- If I don't set isRawShaderMaterial, my code fails here because I used the
  source code from a duplicate MeshPhongMaterial, so it adds extra #define USE_MAP
  etc
