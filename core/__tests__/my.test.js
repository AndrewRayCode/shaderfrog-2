import fs from 'fs';
import parse from 'glsl-parser/direct';
import deparse from '@andrewray/glsl-deparser';
import tokenize from 'glsl-tokenizer/string';
import descope from '@andrewray/glsl-token-descope';
// import Preprocessor from '@andrewray/glsl-preprocessor';

import { MeshPhongMaterialFragment } from '../../shaders/three.fragment.shader';
import { NoiseFragment } from '../../shaders/noise.fragment.shader';
import { RaysFragment } from '../../shaders/rays.fragment.shader';

import {
  shaderNode,
  addNode,
  subtractNode,
  multiplyNode,
  divideNode,
  outputNode
} from '../nodes/nodes';

import {
  frogAstToProgram,
  categorizeAst
} from '../frog-graph-utils/frog-graph-utils';

import { fillHoles } from '../shaderfrog/shaderfrog';

import { without } from '../utils/utils';

describe('some test', () => {
  it('iteration', () => {
    expect('a').toEqual('a');

    const fragment_1 = `
    varying vec2 vUv;
    uniform sampler2D remove_me;
    void main() {
      gl_FragColor = texture2D(remove_me, vUv * t);
		}`;

    const fragment_2 = `
uniform sampler2D filling_shader;
varying vec2 vUv;
uniform sampler2D keep_me;
void main() {
  vec2 q = vUv * 3.0;
  float x = vUv.x * 5.0;
  gl_FragColor = texture2D(keep_me, q * vUv * 3.0);
}
    `;

    const fragment_3 = `
#extension GL_OES_standard_derivatives : enable
precision lowp float;
vec4 tt() {return vec4(1.0);}
void main() {
  gl_FragColor = tt();
}
    `;

    const vertex = `
    void main() {
      gl_Position = 1.0;
    }`;

    // const d = require('glsl-token-depth');
    // const str = `struct IncidentLight {};vec3 y( const in IncidentLight t ) {;}`;
    // const str = `const float d[3] = float[](5.0, 7.2, 1.1);`;
    // const t = descope(tokenize(str), name => 'hi_' + name);
    // console.log('stack', t);
    // const p = parse(descope(tokenize(three)));
    // console.log(np(p.children[0]));
    // console.log(deparse(p));

    // const ssrc = `vec3 x; void main(float x) {x += 1.0;}`;
    // const tq = scope(depth(tokenize(ssrc)));
    // console.log(deparse(parse(descope(tq))));

    //     console.log(
    //       tokenize(`
    // foo() +
    // `)
    //     );

    // console.log(tokenize('false ||1'));

    // const prepd = new Preprocessor().preprocess(MeshPhongMaterialFragment);
    // const outFile = 'mytest.glsl';
    // console.log('output:::\n', trunc(prepd));
    // console.log(`Output written to ${outFile}`);
    // fs.createWriteStream(outFile, 'utf-8').write(prepd);

    // console.log(
    //   cPreprocessor.compile(MeshPhongMaterialFragment, (err, result) => {
    //     console.log('result', err || result);
    //   })
    // );

    //     console.log(
    //       prepr(`
    //   #if OUTER
    //     #if INNER
    //       uniform float a;
    //     #else
    //       uniform float b;
    //     #endif
    //   #endif
    // `)
    //     );
    // console.log(prepr(MeshPhongMaterialFragment));

    const graph = outputNode('gl_FragColor', {
      output: shaderNode(
        MeshPhongMaterialFragment,
        'main',
        'gl_FragColor',
        {
          texture2D_0: addNode({
            0: shaderNode(RaysFragment, 'main', 'gl_FragColor', {}, 2),
            1: shaderNode(NoiseFragment, 'main', 'gl_FragColor', {}, 3)
          })
        },
        1
      )
    });

    console.time('frog');
    const fillerAndAst = fillHoles(graph);
    const printed = deparse(fillerAndAst.fragmentAst);
    console.timeEnd('frog');

    const outFile = 'mytest.glsl';
    console.log('output:::\n', trunc(printed));
    console.log(`Output written to ${outFile}`);
    fs.createWriteStream(outFile, 'utf-8').write(printed);

    console.log('done');
  });
});

const trunc = (str, len = 500) =>
  str && str.length > len ? `${str.substr(0, len)}…` : str;

const nlog = node => {
  let parent = node.parent;
  let output = [];
  let x = 0;
  while (parent) {
    output.unshift(parent.type);
    parent = parent.parent;
    x++;
  }
  console.log(output.join(' > '));
  console.log(
    node && node.length && typeof node === 'object'
      ? node.map(np)
      : node && node.children
      ? without({ ...node, children: node.children.map(np) }, 'parent')
      : node
  );
};

const np = node =>
  node && node.length && typeof node === 'object'
    ? node.map(np)
    : node && node.children
    ? without({ ...node, children: node.children.map(np) }, 'parent')
    : node;
