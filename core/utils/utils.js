export const ky = obj => Object.keys(obj)[0];
export const vl = obj => Object.values(obj)[0];

export const without = (obj, ...keys) => {
  const stringKeys = keys.map(key => key.toString());

  return Object.keys(obj).reduce(
    (memo, key) =>
      stringKeys.indexOf(key) === -1 ? { ...memo, [key]: obj[key] } : memo,
    {}
  );
};

export const indicies = array => array.map((_, index) => index);
