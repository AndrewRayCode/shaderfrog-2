import parse from 'glsl-parser/direct';
import tokenize from 'glsl-tokenizer/string';
import descope from '@andrewray/glsl-token-descope';

const WRONG_THREE = new Set([
  'PointLight', // this is a sstruct name, does it matter?
  'viewMatrix',
  'cameraPosition',
  'toneMappingExposure',
  'toneMappingWhitePoint',
  'diffuse',
  'emissive',
  'specular',
  'shininess',
  'opacity',
  'vColor',
  'vUv',
  'vUv2',
  'map',
  'alphaMap',
  'aoMap',
  'aoMapIntensity',
  'lightMap',
  'lightMapIntensity',
  'emissiveMap',
  'reflectivity',
  'envMapIntensity',
  'vWorldPosition',
  'envMap',
  'envMap',
  'flipEnvMap',
  'maxMipLevel',
  'refractionRatio',
  'vReflect',
  'gradientMap',
  'fogColor',
  'fogDepth',
  'fogDensity',
  'fogNear',
  'fogFar',
  'ambientLightColor',
  'directionalLights',
  'pointLights',
  'spotLights',
  'ltc_1',
  'ltc_2',
  'rectAreaLights',
  'hemisphereLights',
  'vViewPosition',
  'vNormal',
  'directionalShadowMap',
  'vDirectionalShadowCoord',
  'spotShadowMap',
  'vSpotShadowCoord',
  'pointShadowMap',
  'vPointShadowCoord',
  'bumpMap',
  'bumpScale',
  'normalMap',
  'normalScale',
  'normalMatrix',
  'specularMap',
  'logDepthBufFC',
  'vFragDepth',
  'vViewPosition',
  'clippingPlanes'
]);

export const descopeToken = index => name =>
  WRONG_THREE.has(name) ? name : `_${index}_${name}`;

export const descopeTokens = index => tokens => {
  const descoper = descopeToken(index);
  return descope(tokens, descoper);
};

export const mergeAst = (left, right) => ({
  ...left,
  children: [...left.children, ...right.children],
  scope: {
    scope: { ...left.scope, ...right.scope }
  }
});

export const mergeAsts = (base, ...rest) => rest.reduce(mergeAst, base);

export const emptyAst = () => parse(tokenize(''));

export const generateFnParams = src =>
  findAstNode(
    parse(tokenize(`void f(${src}) {;}`)),
    ({ type }) => type === 'functionargs'
  );

export const generateFnArgs = src =>
  findAstNode(
    parse(tokenize(`f(${src});`)),
    ({ type }) => type === 'call'
  ).children.slice(1);

export const renameToken = (ast, node, name) => {
  const newNode = {
    ...junk2js(node),
    token: { ...node.token, data: name }
  };
  if (node.data) {
    newNode.data = name;
  }

  return replaceNodeInAst(ast, node, newNode);
};

export const findAstNodes = (node, match, max = Infinity) => {
  let collection = [];
  collectNodesByPredicate(node, match, collection, max);
  return collection;
};

export const collectNodesInScopeByPredicate = (scope, match, collection) => {
  const keys = Object.keys(scope);
  keys.forEach(key => {
    const thing = scope[key];
    if (key === 'scope') {
      collectNodesInScopeByPredicate(thing, match, collection);
    } else {
      if (match(thing)) {
        collection.push(thing);
      }
    }
  });
};

export const findsInScope = (scope, match) => {
  // console.log(scope);
  let collection = [];
  collectNodesInScopeByPredicate(scope, match, collection);
  return collection;
};

export const findInScope = originalName => ast =>
  ast.scope[
    Object.keys(ast.scope).find(
      key => ast.scope[key].token.descoped === originalName
    )
  ];

export const findDescoped = originalName => ast =>
  ast.scope[
    Object.keys(ast.scope).find(
      key => ast.scope[key].token.descoped === originalName
    )
  ];

export const isDecl = node =>
  node.children[0] && node.children[0].type === 'decl';

export const assignmentTo = name => node =>
  node.children.find(({ type }) => type === 'expr') && node.token.data === name;

export const getFnDef = node =>
  isDecl(node) &&
  node.children[0].children.find(({ type }) => type === 'function');

export const findStmt = (stmtList, matcher) => stmtList.children.find(matcher);

export const filterStmts = (node, matchFn) => node.children.filter(matchFn);

export const replaceOutAssignmentInMain = (
  outVarName,
  mainStatement,
  index
) => {
  const retVarName = `_${index}_ret`;

  // Change the main function return type from "void" to "vec4"
  const voidNode = findAstNode(
    mainStatement,
    node => node.token && node.token.data === 'void' && node.type === 'keyword'
  );
  const vec4 = renameToken(mainStatement, voidNode, 'vec4');

  // Replace the gl_frag* calls with the return variable name
  findAstNodes(
    mainStatement,
    n => n.type === 'builtin' && n.data === outVarName
  ).forEach(node => (node.data = retVarName));

  const mainFnDecl = mainStatement.children[0].children.find(
    ({ type }) => type === 'function'
  );
  const mainFnStmtList = mainFnDecl.children.find(
    ({ type }) => type === 'stmtlist'
  );

  // Create the outstmt (and mutate it out of laziness)
  const newOutStmt = parse(tokenize(`vec4 ${retVarName};`)).children[0];

  // Add the new out declaration first, then the return statement last. The
  // previous logic tried to use the right side assignment of the out var,
  // like gl_FragColor = rightSide <-, but this wouldn't handle cases where
  // gl_FragColor is assigned to directly like gl_FragColor.rgb = vec4();
  return replaceNodeInAst(vec4, mainFnStmtList.children, [
    newOutStmt,
    ...mainFnStmtList.children,
    parse(tokenize(`return ${retVarName};`)).children[0]
  ]);
};

export const findAstNodeAtSourcePosition = (node, line, column) =>
  findAstNode(
    node,
    ({ token }) => token && (token.line === line && token.column === column)
  );

export const findAstNode = (node, match) => {
  if (!node) {
    return;
  }
  if (match(node)) {
    return node;
  }
  for (let i = 0; i < node.children.length; i++) {
    const child = node.children[i];
    const found = findAstNode(child, match);
    if (found) {
      return found;
    }
  }
};

export const findFirstIdentifier = (ast, identifier) =>
  findAstNode(ast, node => node.type === 'ident' && node.data === identifier);

export const findAllIdentifiers = (ast, identifiers, max) =>
  findAstNodes(
    ast,
    node => node.type === 'ident' && identifiers.includes(node.data),
    max
  );

// I wish i knew how to programme
export const collectNodesByPredicate = (node, match, collection, max) => {
  if (match(node)) {
    collection.push(node);
  }
  if (collection.length === max) {
    return;
  }
  if (node.children) {
    node.children.find(child => {
      collectNodesByPredicate(child, match, collection);
      if (collection.length === max) {
        return true;
      }
    });
  }
};

// Undo the Object.create nonsense here: https://github.com/stackgl/glsl-parser/blob/master/lib/expr.js#L255
export const junk2js = node => ({
  ...Object.getPrototypeOf(node),
  ...node
});

export const replaceNodeInAst = (ast, node, replacement) => {
  const stack = getStackTo(ast, node);
  if (!stack) {
    throw new Error('Node not found in ast!');
  }
  return replaceAtStack(ast, ast, stack, replacement);
};

export const getStackTo = (node, find, stack = []) => {
  if (node === find) {
    return stack;
  }
  if (!node.children) {
    return;
  }
  if (node.children === find) {
    return [...stack, 'children'];
  }
  for (let i = 0; i < node.children.length; i++) {
    const child = node.children[i];
    const found = getStackTo(child, find, [...stack, 'children', i]);
    if (found) {
      return found;
    }
  }
};

export const replaceAtStack = (parent, node, [key, ...rest], replacement) => {
  // Case where it's the top level node
  if (key === undefined) {
    return replacement;
  }
  const done = rest.length === 0;
  if (done) {
    replacement.parent = parent;
  }
  if (Array.isArray(node)) {
    return [
      ...node.slice(0, key),
      done ? replacement : replaceAtStack(node, node[key], rest, replacement),
      ...node.slice(key + 1)
    ];
  } else {
    const newNode = {
      ...node
    };
    newNode[key] = done
      ? Array.isArray(replacement)
        ? replacement.map(rpl => {
            rpl.parent = newNode;
            return rpl;
          })
        : replacement
      : replaceAtStack(node, node[key], rest, replacement);
    return newNode;
  }
};
