import parse from 'glsl-parser/direct';
import tokenize from 'glsl-tokenizer/string';
import builtins from 'glsl-tokenizer/lib/builtins';
import operators from 'glsl-tokenizer/lib/operators';
import Preprocessor from '@andrewray/glsl-preprocessor';

import {
  findDescoped,
  replaceOutAssignmentInMain,
  replaceNodeInAst,
  findFirstIdentifier,
  findAstNodes,
  findAllIdentifiers,
  descopeTokens,
  descopeToken
} from '../ast-utils/ast-utils';
import {
  replaceExpression,
  replaceBuiltinCall
} from '../frog-graph-utils/holes';
import { indicies } from '../utils/utils';

export const outputNode = (outVar, fillers) => {
  const src = `
void main() {
${outVar} = a;
}`;
  const indexes = indicies(Object.keys(fillers));
  const ast = parse(tokenize(src)).children[0];
  const holes = {
    output: replaceExpression(findFirstIdentifier(ast, 'a'))
  };
  return {
    type: 'output',
    inputNames: ['a'],
    holes,
    ast,
    fillers,
    exportAst: ast,
    fillerInstruction: {}
  };
};

const preprocessor = new Preprocessor({});

export const shaderNode = (source, mainFnName, outVar, fillers, index) => {
  const descope = descopeTokens(index);
  // const ast = parse(fartSquire(descope(tokenize(source)), descopeToken(index)));
  const ast = parse(descope(tokenize(preprocessor.preprocess(source))));

  const mainDeclaration = findDescoped('main')(ast);
  const renamedMainName = mainDeclaration.data;
  const mainDeclarationStatement = mainDeclaration.parent.parent.parent;

  const replacedMain = replaceOutAssignmentInMain(
    outVar,
    mainDeclarationStatement,
    index
  );

  const replacedAst = replaceNodeInAst(
    ast,
    mainDeclarationStatement,
    replacedMain
  );

  const fillerExpression = parse(tokenize(`${renamedMainName}();`));

  // Tell parent how to fill in their hole with us
  const fillerInstruction = {
    type: 'inline fillerInstruction defined expression',
    expression: fillerExpression.children[0].children[0],
    declaration: replacedMain
  };

  const holes = findAstNodes(
    replacedAst,
    n => n.type === 'builtin' && n.data === 'texture2D'
  ).reduce(
    (memo, expression, idx) => ({
      ...memo,
      [`texture2D_${idx}`]: replaceBuiltinCall(
        expression.parent,
        [{ 1: { backfill: { name: 'vUv', type: 'vec2' } } }],
        []
      )
    }),
    {}
  );

  return {
    type: 'shader',
    ast: replacedAst,
    fillerInstruction,
    holes,
    fillers,
    index,
    exportAst: replacedMain
  };
};

const arithmitickNode = operator => fillers => {
  const indexes = Object.keys(fillers);
  const variables = indexes.map(index => `__${index}`);
  const src = `(${variables.join(operator)});`;
  const parsed = parse(tokenize(src));
  const ast = parsed.children[0].children[0];
  const holes = findAllIdentifiers(ast, variables, variables.length).reduce(
    (memo, hole, index) => ({
      ...memo,
      [indexes[index]]: replaceExpression(hole)
    }),
    {}
  );

  const fillerInstruction = {
    type: 'inline filled in ast'
  };

  return {
    type: operator,
    inputNames: indexes,
    holes,
    ast,
    fillers,
    fillerInstruction,
    exportAst: ast
  };
};

export const addNode = arithmitickNode('+');
export const subtractNode = arithmitickNode('-');
export const multiplyNode = arithmitickNode('*');
export const divideNode = arithmitickNode('/');
