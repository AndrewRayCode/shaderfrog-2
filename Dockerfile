FROM node:alpine

RUN apk update && apk upgrade && \
    apk add --no-cache libc6-compat bash git openssh
ENV PORT 3000
EXPOSE 3000

WORKDIR /app
COPY . /app

RUN npm install

CMD [ "npm", "start" ]
