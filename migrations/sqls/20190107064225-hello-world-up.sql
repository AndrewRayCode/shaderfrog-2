CREATE TABLE IF NOT EXISTS shaders (
    id SERIAL ,
    name varchar(255),
    body text,
    PRIMARY KEY (id)
);
