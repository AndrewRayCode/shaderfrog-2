'use strict';

var dbm;
var type;
var seed;
var fs = require('fs');
var path = require('path');
var Promise;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
  Promise = options.Promise;
};

const horribleEscape = str => str.replace(/'/g, "''");

exports.up = function(db) {
  const shader1 = require('../shaders/three.r100.fragment.shader');
  return db.runSql(`
    INSERT INTO shaders (name, body)
    VALUES (
      'Three.js r100 MeshPhongMaterial',
      '${horribleEscape(shader1.MeshPhongMaterialFragment)}'
    );
`);
};

exports.down = function(db) {
  var filePath = path.join(
    __dirname,
    'sqls',
    '20190112042750-three-r-100-down.sql'
  );
  return new Promise(function(resolve, reject) {
    fs.readFile(filePath, { encoding: 'utf-8' }, function(err, data) {
      if (err) return reject(err);
      console.log('received data: ' + data);

      resolve(data);
    });
  }).then(function(data) {
    return db.runSql(data);
  });
};

exports._meta = {
  version: 1
};
