'use strict';

var dbm;
var type;
var seed;
var fs = require('fs');
var path = require('path');
var Promise;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
  Promise = options.Promise;
};

const horribleEscape = str => str.replace(/'/g, "''");

exports.up = function(db) {
  const shader1 = require('../shaders/three.fragment.shader.js');
  const shader2 = require('../shaders/noise.fragment.shader.js');
  const shader3 = require('../shaders/rays.fragment.shader.js');
  return db.runSql(`
    INSERT INTO shaders (name, body)
    VALUES (
      'Three.js MeshPhongMaterial',
      '${horribleEscape(shader1.MeshPhongMaterialFragment)}'
    );
    INSERT INTO shaders (name, body)
    VALUES (
      'Perlin Noise',
      '${horribleEscape(shader2.NoiseFragment)}'
    );
    INSERT INTO shaders (name, body)
    VALUES (
      'Underwater Rays',     
      '${horribleEscape(shader3.RaysFragment)}'
    );
`);
};

exports.down = function(db) {
  var filePath = path.join(
    __dirname,
    'sqls',
    '20190108051731-some-shaders-down.sql'
  );
  return new Promise(function(resolve, reject) {
    fs.readFile(filePath, { encoding: 'utf-8' }, function(err, data) {
      if (err) return reject(err);
      console.log('received data: ' + data);

      resolve(data);
    });
  }).then(function(data) {
    return db.runSql(data);
  });
};

exports._meta = {
  version: 1
};
