const { ApolloServer, gql, makeExecutableSchema } = require('apollo-server');

const { importSchema } = require('graphql-import');
const { Prisma } = require('prisma-binding');
const path = require('path');

const resolvers = {
  Query: {
    shaders(parent, args, { prisma }, info) {
      return prisma.query.shaders(args, info);
    },
    shadersConnection(parent, args, { prisma }, info) {
      return prisma.query.shadersConnection(args, info);
    }
  },
  Mutation: {
    createShader(parent, args, { prisma }, info) {
      return prisma.mutation.createShader(args, info);
    },
    updateShader(parent, args, { prisma }, info) {
      return prisma.mutation.updateShader(args, info);
    }
  }
};

const typeDefs = importSchema(path.resolve(__dirname, 'schema.graphql'));

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
  allowUndefinedInResolve: false
});

const server = new ApolloServer({
  typeDefs,
  resolvers,
  debug: true,
  allowUndefinedInResolve: false,
  context: req => ({
    ...req,
    prisma: new Prisma({
      typeDefs: path.resolve(
        __dirname,
        '..',
        'prisma',
        'generated',
        'prisma.graphql'
      ),
      endpoint: process.env.PRISMA_URL
    })
  })
});

// This `listen` method launches a web-server.  Existing apps
// can utilize middleware options, which we'll discuss later.
server.listen(process.env.API_PORT).then(({ url }) => {
  console.log(`🚀 API 🐸 Server ready at ${url}`);
});

// const pool = require('../db');

// // This is a (sample) collection of books we'll be able to query
// // the GraphQL server for.  A more complete example might fetch
// // from an existing data source like a REST API or database.
// const books = [
//   {
//     title: 'Harry Potter and the Chamber of Secrets',
//     author: 'J.K. Rowling'
//   },
//   {
//     title: 'Jurassic Park',
//     author: 'Michael Crichton'
//   }
// ];

// // Type definitions define the "shape" of your data and specify
// // which ways the data can be fetched from the GraphQL server.
// const typeDefs = gql`
//   # Comments in GraphQL are defined with the hash (#) symbol.

//   # This "Book" type can be used in other type declarations.
//   type Book {
//     title: String
//     author: String
//   }

//   type Shader {
//     name: String
//     body: String
//   }

//   # The "Query" type is the root of all GraphQL queries.
//   # (A "Mutation" type will be covered later on.)
//   type Query {
//     books: [Book]
//     shaders: [Shader]
//   }
// `;

// // Resolvers define the technique for fetching the types in the
// // schema.  We'll retrieve books from the "books" array above.
// const resolvers = {
//   Query: {
//     books: () => books,
//     shaders: () =>
//       pool.connect().then(client => client.query('SELECT * FROM shaders;'))
//   }
// };

// // In the most basic sense, the ApolloServer can be started
// // by passing type definitions (typeDefs) and the resolvers
// // responsible for fetching the data for those types.
// const server = new ApolloServer({ typeDefs, resolvers });

// // This `listen` method launches a web-server.  Existing apps
// // can utilize middleware options, which we'll discuss later.
// server.listen(process.env.API_PORT).then(({ url }) => {
//   console.log(`🚀  Server ready at ${url}`);
// });
