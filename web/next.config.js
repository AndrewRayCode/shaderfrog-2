// const webpack = require('webpack');

// const rules = (dir, defaultLoaders, isServer) => [
//   {
//     test: /\.(png|gif|jpg|jpeg)$/,
//     use: [
//       {
//         loader: 'file-loader',
//         options: {
//           emitFile: isServer,
//           publicPath: `/_next/static/`,
//           outputPath: `${isServer ? '../' : ''}static/`,
//           name: '[path][name].[ext]'
//         }
//       }
//     ]
//   }
// ];

// module.exports = {
//   webpack: (config, { dir, buildId, dev, isServer, defaultLoaders }) => {
//     const { module, resolve, plugins, resolveLoader } = config;
//     return {
//       ...config,
//       module: {
//         ...module,
//         rules: [...module.rules, ...rules(dir, defaultLoaders, isServer)]
//       },
//       plugins: [
//         ...plugins,
//         new webpack.EnvironmentPlugin({
//           CLIENT_API_URL: process.env.CLIENT_API_URL
//         })
//       ]
//     };
//   }
// };
