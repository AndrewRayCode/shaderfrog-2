import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import dynamic from 'next/dynamic';

import { fillHoles } from '../../core/shaderfrog/shaderfrog';

const TestFrogScene = dynamic({
  loader: () => import('../components/TestFrogScene/TestFrogScene'),
  loading: () => <p>Loading TestFrogScene...</p>,
  ssr: false,
});

export const allShadersQuery = gql`
  query allShaders($first: Int!, $skip: Int!) {
    shaders: shadersConnection(first: $first, skip: $skip) {
      edges {
        node {
          id
          name
          body
        }
      }
    }
    meta: shadersConnection {
      aggregate {
        count
      }
    }
  }
`;
export const allShadersQueryVars = {
  skip: 0,
  first: 10,
};

export default function Index() {
  return <TestFrogScene />;
}
/*
export default function PostList(props) {
  return (
    <Query query={allShadersQuery} variables={allShadersQueryVars}>
      {({
        loading,
        error,
        data: {
          shaders: { edges, aggregate },
          meta: {
            aggregate: { count }
          }
        },
        fetchMore
      }) => {
        if (error) return <div>Error loading posts.</div>;
        if (loading) return <div>Loading</div>;

        const shader = edges.find(
          ({ node: { name } }) => name === 'Perlin Noise'
        );
        const body = shader.node.body;

        const moreShaders = edges.length < count;
        return (
          <section>
            <TestFrogScene three={props.three} fragmentShader={body} />
            <ul>
              {edges.map(({ node: shader }, index) => (
                <li key={shader.id}>
                  <div>
                    id: {shader.id}
                    <br />
                    name: {shader.name}
                    <br />
                  </div>
                </li>
              ))}
            </ul>
            {moreShaders ? (
              <button onClick={() => loadMorePosts(edges, fetchMore)}>
                {' '}
                {loading ? 'Loading...' : 'Show More'}{' '}
              </button>
            ) : (
              ''
            )}
            <br />
            {edges.length} of {count} total shaders.
          </section>
        );
      }}
    </Query>
  );
}

function loadMorePosts(edges, fetchMore) {
  fetchMore({
    variables: {
      skip: edges.length,
      first: 3
    },
    updateQuery: (previousResult, { fetchMoreResult }) => {
      if (!fetchMoreResult) {
        return previousResult;
      }
      return Object.assign({}, previousResult, {
        // Append the new posts results to the old one
        shaders: {
          ...previousResult.shaders,
          edges: [
            ...previousResult.shaders.edges,
            ...fetchMoreResult.shaders.edges
          ]
        }
      });
    }
  });
}
*/
