# How to run

Bring everything up:

```bash
docker-compose up -d
```

Then run the migrations:

```bash
yarn migrate
```

Then generate the `prisma.graphql` schema from the database definition
(`datamodel.prisma`), and generate the GraphQL schema for the internal Prisma
server (which the API later consumes to expose to the world:

```bash
yarn update-prisma
```

The public "GraphQL playground," provided by Apollo Server (used in `api/`) is
[available on port `3001`](http://localhost:3001/).

# Development

## Tests

To run the tests:

```bash
docker-compose run web npm test
```

## Migrations

This project uses the [db-migrate-pg package](https://github.com/db-migrate/pg),
which is the Postgres adapter for
[db-migrate](https://www.npmjs.com/package/db-migrate). This package kinda
sucks, but Prisma doesn't have the same concept of migrations yet, so I'm using
this package. I couldn't find anything better that let me create individual
migrations on Postgres, in Node, without introducing an entire ORM layer.

To create a migration (lol)

```
docker-compose run api npx db-migrate --config migrations/db-migrate.json -e dev create MIGRATION_NAME
```

Note this is just running the db-migrate module inside a container. It's the
db-migrate API.

By default this creates a `migrations/MIGRATION_NAME.js` file, which is
generated Javascript that reads the contents of a generated `.sql` file in
`migrations/sqls`. If you want to just run Javascript and not consume a SQL
file, like `some_shaders.js`, then remove the line `"sql-file": true` from
`migrations/db-migrate.json` and create the migration.

# Infrastructure fixes

I don't understand Docker and a multi-stage build is probably better than what I'm doing.

Some config settings like port `4466` are repeated around the codebase.

Maybe auto-run the `update-prisma` step, or at least the code generation step?

Ah, these are defined in the shader source, so you don't have to figure out what
threejs is doing:

```
parameters.aoMap ? '#define USE_AOMAP' : '',
```

BUT if you're filling in an aoMap, you need to fake some input to threejs to
send that
