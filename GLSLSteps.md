# Welcome back?

function_header

# Todo
- Finish all the parsing
- What is leftAssociate doing again? Do I need it?
- A + parses to {type: '+', chidlren: [left, '+', right]} - can I remove that
  middle child and put whitespace as a key of the top level +
- ✅ Renamed "name" to "type"
- Can I move all trailing whitespace into a ws key instead of in children?
- Impelement printing
- Implement optional whitespace flag
- glsl version switch to support webgl2 vs 1?

# Links

random cpp glsl parser https://github.com/graphitemaster/glsl-parser/blob/main/parser.cpp
microsoft edge glsl lex parser grammar https://github.com/MicrosoftEdge/WebGL/blob/master/GLSLParse/OpenSource/GLSL.y#L378-L385
elimiation of left recursion https://web.cs.wpi.edu/~kal/PLT/PLT4.1.2.html

angle lex file https://github.com/google/angle/blob/master/src/compiler/translator/glslang.l
angle parse file https://github.com/google/angle/blob/master/src/compiler/translator/glslang.y

glsl pest nom PEG blog post https://phaazon.net/blog/glsl-pest-part-1

# Defining all constants

1. Define all the constants, like "INC_OP" and "in"

Questions: what are these operators? Answered
Question: Do I need all of these and translate the grammar verbatim? Or are
there reasons to combine things into one?

# TYPE_NAME

What is TYPE_NAME? From https://github.com/MicrosoftEdge/WebGL/blob/master/GLSLParse/OpenSource/GLSL.y#L378-L385 :

/_ In the GLES grammar, TYPE_NAME is usually covered under constructor_identifier
However, from the tokenization perspective, TYPE_NAME is equivalent to
IDENTIFIER. Because we can't tokenize TYPE_NAME and IDENTIFIER differently,
following the specified grammar would make our grammar ambiguous. Instead,
IDENTIFIER will create a FunctionCallIdentifierNode which begins as
an ambiguous IDENTIFIER. When we verify the parse tree, we'll have enough
context to say whether the identifier is a typename identifier or function
identifier, at which point we'll create the appropriate node. _/

# Start copy pasta'ing the doc into my pegjs grammar file

I found out the PDF at least on macos preview doesn't let you copy paste
lines into an editor properly because it doens't preserve linebreaks. But
if you use the website + chrome, it preserves linebreaks LMAO

# TYPE_NAME

I'm trying setting TYPE*NAME = identifier. ANother option is replacing all
instances of TYPE_NAME \_with* identifier.

# Working on constants like floats, integers

Copying and pasting the liem:
integer-constant :
decimal-constant integer-suffixopt

opt actually means "?" which I almost missed

The spec defines INTCONSTANT, which confused me, because above there is
integer-constant and UINTCONSTANT below. I first thought I needed to rename
integet-constant to INTCONSTANT, but now I'm trying setting INTCONSTANT =
integer-constant and UINTCONSTANT = integer-constant, same for double/float.

# Interface blocks

What is the "interface-block" in the grammar? Its in a separate section but not
in the full grammar below. Does the declarator list include it?

I found the angle grammar (someone who's done it before) and yes it is included
there https://github.com/google/angle/blob/master/src/compiler/translator/glslang.y#600

# field_selection

I was trying to figure out if field_selection needs to be a production of "." identifier
or if it can stand on its own. The yacc grammar I found above made it more
confusing:

https://github.com/google/angle/blob/master/src/compiler/translator/glslang.l#L445-L449

%x FIELDS
<FIELDS>{L}({L}|{D})\* {
BEGIN(INITIAL);
yylval->lex.string = AllocatePoolCharArray(yytext, yyleng);
return FIELD_SELECTION;
}

%x is an "exclusive start condition" https://www.ibm.com/support/knowledgecenter/en/ssw_aix_71/generalprogramming/lex_program_start.html

OH this shit explains it https://docs.oracle.com/cd/E19504-01/802-5880/lex-71437/index.html

"." { BEGIN(FIELDS); return DOT; }

this rule puts the lex pareser into the FIELDS state, and the above rule <FIELDS>
is only matched if it's in that state. Then it resets back to INITIAL

# Microsoft

Microsoft edge glsl parser lex grammar file
https://github.com/MicrosoftEdge/WebGL/blob/master/GLSLParse/OpenSource/GLSL.y#L378-L385

# De left recursing

When looking at this page https://web.cs.wpi.edu/~kal/PLT/PLT4.1.2.html
It has the instructions:

For each rule which contains a left-recursive option,

A --> A alpha|Beta
introduce a new nonterminal A' and rewrite the rule as
A --> Beta A'
A' --> empty | alpha A'

What has been throwing me off is the ordering of these things. Forest pointed
out that | is commutitive, so in this hellscape,

A --> A alpha|Beta
and
A --> Beta|A alpha

are equivalent. In PEG grammars however, / is not commutitive, so order matters.
In my case,

decimal_constant
= nonzero_digit
/ decimal_constant digit

Should be A --> Beta | A alpha

and then apply the formula. And the second thing is that empty needs to go at
the END of my PEG grammar. Now the final de left recrused matching rule makes
sense, at least after applying step 1.

# De left-recursing

I did some "easy" ones, like

statement_list
= statement
/ statement_list statement

into

statement_list = statement+

# suffix for left recursing

And then a "harder" one, init_declarator_list, where I factored out a suffix
rule, then factored IDENTIFIER out, then had to put in an empty case to make
the rule equivalent. Here I think this works, Forest confirmed, but I'm also
unclear what other strategies I have to solve this.

# relational_expression (operation factoring out)

I factored out \_suffix

Then forest suggested I could make relational_op = the middle

# grek

β is lowecase lol

# onine left recursion

https://cyberzhg.github.io/toolbox/left_rec

BOTH of these

translation_unit -> external_declaration | translation_unit external_declaration

de left recurse to

translation_unit -> external_declaration translation_unit'
translation_unit' -> external_declaration translation_unit' | ϵ

I'm trying to figure out if
A = β / Aa
and
A = Aa / β
de left recurse to the same thing
Forest: Yes

# What does "constructor" mean?

// Grammar Note: Constructors look like functions, but lexical analysis
// recognized most of them as keywords.

Oh, constructor is like vec3(), makes sense that it would be recognzied as a
keyword.

Possible infinite loop when parsing (left recursion:
postfix_expression
function_call
function_call_or_method
function_call_generic
function_call_header_with_parameters
function_call_header
function_identifier
postfix_expression
)

# What is a postfix_expression?

unary_expression
= postfix_expression

what is a unary expression for that matter? Expression with only
one "operand" and a unary operator. "1 +" is a unary expression
which might be used in "1 + 1" later.

Postfix can be !(expression) for example. "++" and "--" are
postfix expressions. So is [1] in an array lookup.

Trying to figure out why a function_identifier needs a
postfix_expresion. Ah, the grammar note tells why. There's
.length() now? Ok, .length is one case. WTF is a subroutine
array call? ooooh you can do some special case bullshit
subroutineName[1](). And finally identifiers. Learning about
what these rules actulaly mean gave me the tools to update the
grammar to handle these cases.

So I ended up with the new function_identifer rule, dunno if
it's right (or good enough) yet:

function_identifier
= type_specifier
// Handle the .length() case
/ DOT FIELD_SELECTION
// Handle the subroutine array call case, and the identifier case
/ IDENTIFIER (LEFT_BRACKET integer_expression RIGHT_BRACKET)?

# dingus

possible left recursion:
statement
simple_statement
iteration_statement
statement_no_new_scope
simple_statement

oh it was a typo thank god

----- PARSER BUILT SUCCESSFULLY ----

# wtf it doesn't work at all

debugging, INTCONSTANT is matching before FLOATCONSTANT on 1.2. Why? Oh, because
primary_expression = int / float, and it matches the first part of int and then
fails. So I have to switch their order.

Ok I changed their order

oh GOD I DIDN'T ACCOUNT FOR WHITESPACE

Adding whitespace to end of every terminal

OH GOD I DIDN'T ACCOUNT FOR COMMENTS

Adding comments too

Now all terminals return an array

super verbose fix but it works

# lol parsing

lol it parsed (1+2) however it made a hugely nested series of arrays

# left associativity

a ~ b ~ c. If the operator ~ has left associativity, this expression would be interpreted as (a ~ b) ~ c

GLSL grammar calls this "Left to Right"

# right associative?

Make sure I handle "right to left" associativiy in the grammar so that
a = b = c parses to a = (b = c)

After a lot of work I have arrived on a potential leftAssociate and rightAssocaite functions.
The tricky thing for me was figuring out how the right associate works, and it
turns out it needs a different step for the start, where it has to merge
children under the operator like "+": children: [], but the subsequent steps
don't merge children, they keep left children and add right as final child

# Whitespace FML

What made the above even more complicated for me was trying to figure out how to
handle whitespace. Because if I parse "1+ 2" and at first I get

```json
{
  "name": "group",
  "children": [
    {
      "name": "+",
      "children": ["1", "2"]
    }
  ]
}
```

I can't put the whitespace in the children ["1", " ", "2"] because it's
ambiguous if the whitespace goes

So I explored some options

```json
{
  "name": "group",
  "trailingWhitespace": " ",
  "leadingWhitespace": " ",

  // or maybe
  "prefix": "( ",
  "suffix": "( ",

  "children": [
    {
      "name": "operator",
      "operator": {
        "name": "+",
        "trailingWhitespace": " "
      },
      "children": ["1", "2"]
    }
  ]
}
```

The problem with these is that I'm making special case node keys, like prefix,
suffix, operator.

After talking with John and Oliver, I'm going to try...

Tried to read Prettier whitepaper https://homepages.inf.ed.ac.uk/wadler/papers/prettier/prettier.pdf
I was unable to understand it so far

I converted every terminal node to having optional trailing whitespace in its
children, and also including operator nodes like "+" in the children of the "+"
ast node. So now if I want left and right of a plus node it's chidlren[0] and
children[2] becuase the middle node is the + with optional whitespace in there.
Same for paren groups. I may want to make whitespace its own node as a sibling
to the + node, i don't know yet.

What I was going back and forth on was could I encode whitespace in a way for
the AST that when I traversed it I could optionally deal with whitespace. This
solution means I still have to be aware the first child of a "group" node is
"(" with potential whitespace in its children, but if I don't dive into the
children of "(" a walker could essentialy treat it as a terminal node.

# whitespace woes

i'm working on functions. a function declaration can be something like "void
identifier(...)" in my grammar all of my terminals have optional whitespace
after them, so void is something like "void \_?"

my production needs to enforce whitespace between "void" and "identifier". I
can't do "type \_ identifier" because the type terminal will have already
captured the whitespace

if I do "declaration = type identifier" without whitespace between, then my
parser will happily consume "voididentifier" which isn't valid glsl

i could enforce the whitespace exists after void manually in the javascript
expression executed for the match, but that's awkward and i'll have to do that
in a few places

# negaive lookaheads

oliver pointed out I can end terminals in a negative lookahead. So "void" can be
enforced to be "void without an [a-zA-Z] after it". This is great because it
means that the production "void identifier" will never match the identifier
"voidMyName" because the "void" terminal requires a non a-z after it to be a
keyword.

voidMyname is also a general identifer problem independent of whitespace. void
![a-z] fixes that too, identifiers can start with keywords and be fine.

# Aside on changing production orders and hijacking

I have to do lots of rule switching around to fix specificity and other rules
"hijacking" previous rules. Like a "function header" can hijack a "function
header with parameters" if it comes first, because it matches "name(" which can
start either type. So I have to swap them.

# Collapsing rules

The grammar has this:

parameter_declaration:
type_qualifier parameter_declarator
parameter_declarator
type_qualifier parameter_type_specifier
parameter_type_specifier

parameter_type_specifier:
type_specifier

I've collapsed it to

parameter_declaration
= type_qualifier? (parameter_declarator / type_specifier)

Is there a significance to having a separate production for

parameter_type_specifier:
type_specifier

instead of using type_specifier?

Same quesiton for

simple_statement:
declaration_statement
expression_statement
...

then

declaration_statement:
declaration

Oliver suggested this is just for naming consistency. aka just being polite. I'm inlining these. I don't know if there are other cases.

# maybe they are significant for parsing?

compound_statement_no_new_scope:
LEFT_BRACE RIGHT_BRACE
LEFT_BRACE statement_list RIGHT_BRACE

is identical to

compound_statement:
LEFT_BRACE RIGHT_BRACE
LEFT_BRACE statement_list RIGHT_BRACE

the first one is used for function bodies.

But why are there two of these?

# Grammar errors?

A initilalizer list can end with "comma identifier = initializer"
where initializer can be an assignment expresion.
oh shit this is required for
float a = 1.0, b = c = 1.0;

# Parse tree

float a = 2.0, b = 1.0, c;

What should this parse as?

{
type: 'declaration_lsit',
children: [
{ type: 'single_declaration', float a = 2.0 },
','
{ type: 'single_declaration', b = 1.0 },
','
{type: 'single_declaration', c }
]
}

# No attribute?

Why is there no "attribute" in the grammar? Is it because it's replaced with "in" ?

# Comparing

single_declaration

type_qualifier? (mediump)
type_specifier (float[1])
identifier ( a )
array_specifier ( [1] )

to

init_declarator_list_suffix

identifier ( a )
array_specifier ( [1] )
equal
initialier

# Note on making the / "" rule

Original comment:

// From the left recursive rule, we factored out the suffix rule to remove
// the left recursion. Then from that we factored out IDENTIFEIR into the
// main rule. Because of that, we removed the = IDENTIFIER case from this
// suffix rule, so we need to handle the empty case, at the _end_ of this
// list, otherwise it will always match first
// float a = 1.0, b;
/ ""

But now that we use PEG to do ? we don't need this

# Rename

I renamed `single_declaration` to `initial_declaration` and the other one to
`subsequent_declaration` after seeing how similar they were

# Made new rule

precision_declarator

# Why

```js
declaration
  init_declarator_list SEMICOLON
  type_qualifier IDENTIFIER LEFT_BRACE struct_declaration_list RIGHT_BRACE SEMICOLON
```

```js
init_declarator_list ->
initial_declaration ->
  // fully_specified_type =
  // type_qualifiers (const, mediump, in, etc), type_specifier
  fully_specified_type ->
    type_specifier -> type_specifier_nonarray ->
    struct_specifier -> STRUCT IDENTIFIER LEFT_BRACE struct_declaration_list
```

```js
initial_declaration =
  fully_specified_type (IDENTIFIER array_specifier? (EQUAL initializer)?)?

initial_declaration =
  type_qualifiers? type_specifier (IDENTIFIER array_specifier? (EQUAL initializer)?)?

initial_declaration =
  type_qualifiers? (type_specifier_nonarray array_specifier?) (IDENTIFIER array_specifier? (EQUAL initializer)?)?

initial_declaration =
  type_qualifiers? ((struct_specifier / TYPE_NAME)) array_specifier?) (IDENTIFIER array_specifier? (EQUAL initializer)?)?

initial_declaration =
  type_qualifiers? ((struct_specifier / TYPE_NAME)) array_specifier?) (IDENTIFIER array_specifier? (EQUAL initializer)?)?

// And finally
struct_specifier
  = STRUCT IDENTIFIER LEFT_BRACE struct_declaration_list RIGHT_BRACE
  / STRUCT LEFT_BRACE struct_declaration_list RIGHT_BRACE
```

vs?

```
type_qualifiers IDENTIFIER LEFT_BRACE struct_declaration_list? RIGHT_BRACE SEMICOLON
```

What is the meaning of type_qualifier here? should that just be put into
init_declarator list above?

```glsl
  struct S { float f; };
  struct T {
    S; // Error: anonymous structures disallowed
    struct { ... }; // Error: embedded structures disallowed
    S s; // Okay: nested structures with name are allowed
  };
```

I see I think this is for "interface blocks" which don't have initializers at
the end and have limited qualifiers at the beginning

# notes on the structure

the document mentions lots of "compile time errors" that are context dependent
and not present in the grammar.

# random aside

if i want to rename variables in this AST, I must consider the case:

```glsl
in Light {
  vec4 LightPos;
  vec3 LightColor;
};
```

Where the shader accesses `LightPos` only at the top level. That will need to
get renamed. Not sure about Light since that has implications for other shader
stages.

# debugging peg

I set start rule to something

console.log('production_name', { some_thing });

Using online editor

```js
start = debugstart
debugstart = start:initial_declaration {
  console.log('------- done');
  return start;
}
```

# Working on structs

another example where I decided to add a node, struct_field_declarator
and a 'struct_stmt' to preserve trailing whitespace

# I found the paper

https://pdos.csail.mit.edu/papers/parsing:popl04.pdf it's great and also puts
whitepsace after terminals by default

# if else

How should if / else if / else be parsed?

# switch statement

    switch_statement:
      SWITCH LEFT_PAREN expression RIGHT_PAREN LEFT_BRACE switch_statement_list
      RIGHT_BRACE
    switch_statement_list:
      /* nothing */
      statement_list

Why isn't the body a compound_statement? I guess it's different, it's not a statement, it's a body

Grammar note:

// Grammar Note: labeled statements for SWITCH only; 'goto' is not supported.

I was confused about the first part, but Oliver asked a good question. And the answer is

# Duff's device

Thank god, the "duff's device" isn't valid

    switch(x) {
      while (false) { case 4: 3; }

errors with two errors:

    ERROR: 0:19: 'case' : label statement nested inside control flow
    ERROR: 0:18: 'switch' : statement before the first label

# Declaraiton history with semicolon

// TODO: Statement parsing
simple_statement
--  = declaration_statement
++  = declaration // Note declaration ends in semicolon
++  / SEMICOLON

-- declaration_statement
--   = declaration

aFunction()

    declaration_statement
      = declaration

    external_declaration
      = function_definition
      / declaration

declaration
-> init_declarator_list SEMICOLON
-> initial_declaration

# Thinking

I had nodes where "what the node was called" key was "name." I just changed this
to "type" which makes more sense based on my current understanding of how I want
to do AST walking in the future. I have node _types_ that have shapes that are
unique to that node, so I don't have to re-parse the structure. I'm trying
to think of an example - like if I had some list of elements as children I
parsed, and some where significant - in a walker I'd have to go check every
child for significance. VS if they're structured nodes, I can just check if
that field exists. Maybe like if parens are optional in a group, I'd rather
not check if(children[0] === '(') in a walker, I'd rather check (if children.op)
for open paren. I'm not sure if that's a good example. The principle I'm
using here is "retain parse information" and not discard it, so future parts of
the program don't have to re-check for it.

# Do I need to match the Khronos grammar 1:1?

No, I don't think so now! More info in this thread https://community.khronos.org/t/question-about-glsl-grammar-compared-to-desired-ast-output/107315/5 which I
asked about `function_call_or_method` and `function_call_generic`. 

from Oliver:
> Because many productions are trying to work around limitations of EBNF
> e.g ebnf doesn’t let you say parameter_list : (type name (, type name)*)?
> But other language grammars do

# Looking at a lecture on compilers
https://web.stanford.edu/class/archive/cs/cs143/cs143.1128/
Wow looks like a great resource

- Lexing ("scanning"), making tokens
- syntax analys ("parsing"), identify how the tokens relate to each other
- semantic analysis: identify meaning (this is where you would do type checking,
  making sure variables aren't used before they're defined...)
- "IR" generation - generate a "possible structure". In his example he turns an
  AST of a while loop into assembly-like instructions
- "IR" optimization - simplify the structure
- Generation (could be code generation) - build the structure
- Optimization - Optimize the code

"lexeme" is the piece of the program we used to make the token

regular expressions can be implemented using "finite automata", which can be
- NFA (nondetereministic finite automata)
- DFA (deterministic finite automata)

NFA example diagram has a double circle showing an "accepting state" - string is
accepted if the parsing ends in this state.

Python scanning - he says since white space is significant there can be an
INDENT and DEDENT token. In other langauges, these would be "{" and "}" and ";"

# Parsing "a.length()"

A postfix_expression can be a function_call
A function_*identifier* can be a postfix_expression

.length(), which is the function identifier, is itself a postfix expression.
It's function_identifier = postfix_expression

So WHY can a postfix_expression be a function_call???
a.length() should get analyzed as a function_call

Is it to support a.length()[1] <-- where .length() is a function call
And to support primary_expression.postfix_suffix?

It's because the whole THING, .length() ITSELF is the function CALL, not the
                              ‾‾‾‾‾‾‾‾‾
function IDENTIFIER. So postfix_expression is a valid rule, I think I need to
kill the recursion in function_identifier - except ... "variable_identifier"
(inside of "primary_expression" in postfix_identifeir belongs to the postfix,
not to the function/method identifier. So I'm not sure if I can use 
"variable_identifeir" in function expression without doing a terrible hack

Ok time for bed. 

a(a()).length()
‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ < function_call left_paren
‾‾‾‾‾‾‾‾‾‾‾‾‾   < function_identifier
‾‾‾‾‾‾‾‾‾‾‾‾‾   < postfix_expression
‾‾‾‾‾‾‾‾‾‾‾‾‾   < function_call dot field_selection
‾‾‾‾‾‾          < function_call
‾               < function_identifier

# Diverging from the khronos grammar

According to the khronos grammar, a function_identifier can be a postfix_expression which means that it could be a.length++.length - which of course it can't. So I just inlined the postfix_expressio chunks into the function identifier I needed. 

This comment was added to the end of the grammar in a later version of the PDF
than I started with:

> In general the above grammar describes a super set of the OpenGL Shading
> Language. Certain constructs that are valid purely in terms of the grammar are
> disallowed by statements elsewhere in this specification.

Makes sense, especially for postfix_expression

# integer_expression and grammar understanding

integer_index = lb:LEFT_BRACKET expr:integer_expression rb:RIGHT_BRACKET

integer_expression
  = expression

This is probably done as a compiler helper to say this has to be an expression that returns an integer. It might even be helpful to capture this information!

# Javascript visitng AST tutorial

https://lihautan.com/manipulating-ast-with-javascript/

OpenGL ES and WebGL use OpenGL ES Shading Language (abbreviated: GLSL ES or ESSL).

# Versions
from https://en.wikipedia.org/wiki/OpenGL_Shading_Language

┌-----------------┬-------------------┬---------------┬-----------------------┬-----------------┬---------------------┐
| GLSL ES version | OpenGL ES version | WebGL version | Based on GLSL version | Date            | Shader Preprocessor |
├-----------------+-------------------+---------------+-----------------------+-----------------+---------------------┤
| 1.00.17[14]     | 2.0               | 1.0           | 1.20                  | 12 May 2009     | #version 100        |
| 3.00.6[15]      | 3.0               | 2.0           | 3.30                  | 29 January 2016 | #version 300 es     |
└-----------------┴-------------------┴---------------┴-----------------------┴-----------------┴---------------------┘

Uhh, wtf? There's the OpenGL ES Shading Language Version 3.00: https://www.khronos.org/registry/OpenGL/specs/es/3.0/GLSL_ES_Specification_3.00.pdf

and the older OpenGL ES Shading Language Version 1.00: https://www.khronos.org/registry/OpenGL/specs/es/2.0/GLSL_ES_Specification_1.00.pdf

What is the difference between GLSL ES and the OpenGL Shading Language Version?
GLSL ES version is BASED on a GLSL version...

OpenGL ES 3.0 vs 2.0 and backwards compatability?

https://www.shaderific.com/blog/2014/3/13/tutorial-how-to-update-a-shader-for-opengl-es-30

OpenGL ES 3.0 is fully backward compatible with OpenGL ES 2.0 which means that all your shaders targeted at OpenGL ES 2.0 keep running just fine.

    #version 300 es

Tutorial - How to update a shader for OpenGL ES 3.0
https://www.shaderific.com/blog/2014/3/13/tutorial-how-to-update-a-shader-for-opengl-es-30

From the spec:

Version 1.10 of the language does not require shaders to include this directive,
and shaders that do not include a #version directive will be treated as
targeting version 1.10. Shaders that specify #version 100 will be treated as
targeting version 1.00 of the OpenGL ES Shading Language. Shaders that specify
#version 300 will be treated as targeting version 3.00 of the OpenGL ES Shading
Language. Shaders that specify #version 310 will be treated as targeting version
3.10 of the OpenGL ES Shading Language.

# Babel

Babel plugin handbook https://github.com/jamiebuilds/babel-handbook/blob/master/translations/en/plugin-handbook.md

# Open source

# Now fighting postfix again

a().length(); works
texture(a()).rgb; now fails because "texture(a()).rgb" gets consumed as the
  function identifier.

I did it by inlining the left paren into the function header itself to verify
it gets matched, and found

(a / b) c
is different than
a c / b c
beacuse a could consume with out c, then fail, when it shouldn't

# handling of "float" and keywords

I make a token right now of type: "float". Should this be a keyword instead?

# stuff

I renamed "selection_statement" to "if_statement" which seems easier to parse

After consulting with oliver, I feel confident in making a list of switch:
[cases] at parse time, instead of semantic analysis.

# glsl es 1.0 vs glsl es 3.0

From https://github.com/KhronosGroup/WebGL/issues/2821

In GLSL ES 1.0 you can not do

float array[3] = float[3](1.0, 2.0, 3.0);

But you can in GLSL ES 3.0.

In fact in WebGL1 we get these error from ANGLE

ERROR: 0:13: '[]' : array constructor supported in GLSL ES 3.00 and above only
ERROR: 0:13: '[]' : first-class arrays (array initializer) supported in GLSL ES 3.00 and above only

Note: you can do this in GLSL 1.0

 float array[3];
 array[0] = 1.0;
 array[1] = 2.0;
 array[2] = 3.0;

# inline of arrays of data vs associativity

With declarations, you can do things like float a = 1, b = 2, c, d;

In my original design, I interprieted this as the comma operator, and made the
resulting ast be a right associative comma tree. after looking at the estree
output for javascript's switch/case and declaration, i saw that they nicely
updated the data structure to be arrays of case statements, and declarations,
and not trees. i copied that behavior, which i now use for declarations,
switch statements, and layout qualifiers. this has enabled me to understand the
mapping of grammar to ast a little better.

# semantic analysis

the switch/case statement has introduced me to semantic analysis, where switch
bodies can't start with anything other than case, and can't end with case. I
could encode this in the grammar but for now did it in the function that runs
over the ast nodes.

# Preprocessor

lol wtf am I doing trying to write a preprocessor

semi-helpful preprocessor docs that vaguley reference the spec https://docs.microsoft.com/en-us/cpp/preprocessor/grammar-summary-c-cpp?view=msvc-160

The C++ standard (cpp) is 1325 pages long lol http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2010/n3092.pdf

oh thank god glsl can only handle integers in preprocessor limits things a lot

https://gcc.gnu.org/onlinedocs/gcc-3.3.6/cpp/The-preprocessing-language.html

- a preprocessor directive can't cover more than one line, but it can end with a
  backslash, or a block comment that spans more than one line

I figured out how to do expression parsing.

I learned about the toking pasting operator, The "stringize" operator, variadic
macros, function-like macros, object-like macros.

# Years later

11 months ago I made the first commit for the glsl pegjs grammar.
Today on august 28th 2021, the day before my 35th birthday, after a lovely day
in golden gate park with Bun and shade tent, I'm returning to this repo to see
if I can use any of the parser here.

I found out about things in the grammar I was confused about, like type_name,
compound_statement_no_new_scope, and why those are relevant for parsing.

I came across the realization that you can do lots of things in a parser. You
can throw errors on undefined variables to prevent ambiguous parses, or you can
warn and hope for the best.

# Months later

June 2nd, 2022. I have used the parser. By the way, you might want the frogger/
repo, not this one.

# Months later

November 12, 2022. Copying some debugging logs out of shaderfrog. Have had less
time to work on Shaderfrog now that IH work is ramping up again.

Hit an issue while adding textures as data nodes to the graph. Before I was hard
coding texture uniforms in ThreeComponent. Now I want them as data nodes. To do
that, this uniform strategy needs to include sampler2D uniforms. It *didn't*
before because there was no support for texture data nodes, and because the
texture2D strategy handles the texture2D calls loading the sampler2D.

So I commented this out to allow for sampler2D uniforms to appear as inputs. But
NOW the sampler2d "map" line in the physical shader is overwriting the "map"
PROPERTY input on the shader (both are mapped to "albedo") so when I plug in a
shader into "albedo" now it triggers the uniform filler (previously sampler2d
filler) which makes the invalid line "texture(purple_metal(), vUv)"

So if there's an albdeo UNIFORM filler which replaces instances of that uniform
with a filler, (by the way that SUCKS and this should only inject the filler
once if possible), AND a texture2D filler, then... uh... hold on.
 - before: albedo was sampler2d filler (or becomes property setter if the
   category input is "data")
 - now: albedo is uniform filler and plugging in code slaps it in the wrong
   spot. I want sampler2D uniforms to show up so I can add data into them. So I
   need albedo_uniform_data/albedo_uniform_code/albedo_sampler2d_code? You can't
   put data into the sampler2d code filler, it's only bakeable I don't see it
   right now, bedtime
   
As an aside maybe the sampler2D strategy isn't what I want in the sense that it
finds three noiseImage inputs on the perlin clouds shader ... although that
seems right, because that's all different uv lookups.

... so now i've added an "accepts" property to inputs to say if they accept data
or code. I've moved "category" into "baked" and "bakeable". I added image data
nodes to the graph. I moved proeprty setting out of hard coding in
ThreeComponent. I added "type" to inputs (uniform | property | filler). Made IDs
more unique on inputs because a map texture() filler is not the same as a map()
uniform filler is nto the same as a map() property.

All of this is to allow a texture and a shader to be plugged into albedo.

Now in the graph plugging in thickness does nothing lol. Albedo works as a
shader, but after baking, plugging in shader, it can't be un- baked? Need
auto-baking too when dragging a shader or texture into an input. And now we have
two inputs named map. Had shower thought: for "albedo" is there a higher
grouping object that hides multiple inputs behind the group? "special:
["filler_map" | "property_map"]"? I don't like that a property has a fillerName,
it would be nice if something else knew about that relationship.

wtf why isn't transmission working now

# July 16 2023

- Wanted to add SSS shader example
- Realized this isn't compatible with setup
- Went to add inject strategy
- Realized the parser didn't support undefined variables
- Started working on the parser, realized it wasn't in Typescript and was
  getting anoyed at it and how hard it was to write code
- Realized the parser didn't handle function overloads which complicated the variable case, added those
- Finished the typescript update
- Realized I had a bigger issue with type vs variable handling, fixed that, added tests
- Started using new compiler beta version in shaderfrog, found a bunch of bugs
- Fixed the bugs
- Used only part of the new compiler in Shaderfrog
- Had to fix major typeerror in graph
- Which lead to a circular dependency causing build failure, fixed it
- Trying to deploy server revealed broken asset and path issues
- Then nextjs wouldn't start in production mode and I had to redo my pm2 setup